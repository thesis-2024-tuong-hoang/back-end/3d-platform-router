FROM node:20-alpine

WORKDIR /app

COPY . .

RUN yarn install

EXPOSE 3002

CMD [ "yarn", "dev" ]