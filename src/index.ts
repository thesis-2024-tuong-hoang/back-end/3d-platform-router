import express from 'express'
import logger from 'morgan'
import authRouter from './routes/auth'
import fileRouter from './routes/file-processor'
import dataRouter from './routes/data-processor'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import errorHandler from './middlewares/error'

const app = express()

const corsOptions = {
    origin: '*',
    methods: ['GET, POST, PUT, DELETE, PATCH'],
    allowedHeaders: ['Content-Type, Authorization, Authorization-1, Authorization-2'],
}

app.use(cors(corsOptions))

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use('/auth', authRouter)
app.use('/process-file', fileRouter)
app.use('/process-data', dataRouter)

app.use(errorHandler)

console.log('Router running on localhost:3002')

app.listen(3002)
