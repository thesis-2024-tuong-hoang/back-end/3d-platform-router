import supabase from '~/constants/supabaseConfig'
import { ITokenRecord } from '~/type'

class TokenServices {
    static async createNewToken(tokenRecord: ITokenRecord) {
        const response = await supabase.from('tokens').insert(tokenRecord)
        return response
    }

    static async deleteTokenByUserId(userId: string) {
        const response = await supabase.from('tokens').delete().eq('user_id', userId)
        return response
    }

    static async getTokenByUserId(userId: string) {
        const { data: token } = await supabase.from('tokens').select('*').eq('user_id', userId)
        return token![0]
    }

    static async refreshTokens() {}
}

export default TokenServices
