import supabase from '~/constants/supabaseConfig'
import { ErrorResponse } from '~/utils/errors'
import { Result, ValidationError } from 'express-validator'
import { IUserInfo, IUserRegister, IUserLogin } from '~/type'
import CredentialServices from './credentialServices'
import TokenServices from './tokenServices'
import { hash, compare } from 'bcryptjs'
import JWT from '~/utils/jwt'

class UserServices {
    private static getUsersByEmail = async (email: string) => {
        const { data: users } = await supabase.from('users').select('*').eq('email', email)
        return users
    }

    private static getUserByIdAfterCreated = async (newUser: IUserInfo) => {
        const { data: users } = await supabase.from('users').insert(newUser).select('id')
        return users
    }

    static async register(registerInput: IUserRegister, errorsInput: Result<ValidationError>) {
        const { email, password, firstname, lastname } = registerInput
        if (!errorsInput.isEmpty()) {
            const error = ErrorResponse('Validation failed', 409, errorsInput)
            throw error
        }

        try {
            const user = await this.getUsersByEmail(email)
            if (user!.length > 0) {
                const error = ErrorResponse('Email is already in use.', 409)
                throw error
            }

            const newUser = await this.getUserByIdAfterCreated({
                firstname,
                lastname,
                email
            })

            const hashedPassword = await hash(password, 12)
            const response = await CredentialServices.createUserCredential({
                password: hashedPassword,
                email,
                user_id: newUser![0].id
            })
            return response
        } catch (error: any) {
            if (!error) {
                error.status = 500
            }
            throw error
        }
    }

    static async login(loginInput: IUserLogin, errorsInput: Result<ValidationError>) {
        const { email, password } = loginInput
        if (!errorsInput.isEmpty()) {
            const error = ErrorResponse('Validation failed', 409, errorsInput)
            throw error
        }

        try {
            const user = await this.getUsersByEmail(email)
            if (user?.length == 0) {
                const error = ErrorResponse('Could not find your account with this email', 404)
                throw error
            }
            const userCredential = await CredentialServices.getCredentialByUserId(user![0].id)

            const isMatched = await compare(password, userCredential.password)
            if (!isMatched) {
                const error = ErrorResponse('Validation failed', 409, undefined, 'Wrong password. Please try again')
                throw error
            }

            const newAccessToken = JWT.generateJWT(
                { userId: user![0].id, name: user![0].firstname + ' ' + user![0].lastname },
                'access'
            )

            const newRefreshToken = JWT.generateJWT(
                { userId: user![0].id, name: user![0].firstname + ' ' + user![0].lastname },
                'refresh'
            )

            if (newRefreshToken) {
                await TokenServices.createNewToken({ token: newRefreshToken, user_id: user![0].id })
            }

            return {
                accessToken: newAccessToken,
                refreshToken: newRefreshToken,
                status: 200
            }
        } catch (error: any) {
            if (!error) {
                error.status = 500
            }
            throw error
        }
    }
}

export default UserServices
