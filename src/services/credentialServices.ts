import supabase from '~/constants/supabaseConfig'
import { IUserCredential } from '~/type'

class CredentialServices {
    static async createUserCredential(userCredential: IUserCredential) {
        const response = await supabase.from('credentials').insert(userCredential)
        return response
    }

    static async getCredentialByUserId(id: string) {
        const { data: credentials } = await supabase.from('credentials').select('*').eq('user_id', id)
        return credentials![0]
    }
}

export default CredentialServices
