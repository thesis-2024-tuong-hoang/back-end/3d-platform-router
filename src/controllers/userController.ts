import 'dotenv/config'
import * as express from 'express'
import { IUserRegister, IUserLogin, ILoginResponse } from '~/type'
import { AUTHENTICATION_URL, DATA_PROCESSOR_URL } from '~/constants/constants'
import axios from 'axios'

// interface IModelBody {
//     description: string
//     name: string
//     userId: string
// }

class UserController {
    static async getUserById(req: express.Request, res: express.Response, next: express.NextFunction) {
        const body = req.body as any

        console.log(body.userId)

        try {
            const response = await axios({
                method: 'post',
                url: DATA_PROCESSOR_URL + '/user',
                data: {
                    userId: body.userId
                }
            })

            // console.log(response)

            if (response.data.status === 200) {
                res.status(response.status).json({
                    user: response.data.user
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async getUserBySlug(req: express.Request, res: express.Response, next: express.NextFunction) {
        const userSlug = req.params.userSlug as string

        try {
            const response = await axios({
                method: 'get',
                url: DATA_PROCESSOR_URL + `/user/${userSlug}`
            })

            // console.log(response)

            // console.log(response)

            if (response.data.status === 200) {
                res.status(response.status).json({
                    user: response.data.user
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async updateUserById(req: express.Request, res: express.Response, next: express.NextFunction) {
        const bodyUserData = req.body as any
        const fullAccessToken = req.get('Authorization-1')?.split(' ')[1]
        const fullRefreshToken = req.get('Authorization-2')?.split(' ')[1]
        
        try {
            const authorizeResponse = await axios({
                method: 'post',
                url: AUTHENTICATION_URL + '/authorize',
                data: {
                    accessToken: fullAccessToken,
                    refreshToken: fullRefreshToken
                }
            })

            if (authorizeResponse.data.isAuthorized) {
                const response = await axios({
                    method: 'put',
                    url: DATA_PROCESSOR_URL + '/user',
                    data: bodyUserData
                })
    
                if (response.status === 204) {
                    res.status(response.status).json({
                        status: 204
                    })
                }
            }
        } catch (error) {
            next(error)
        }
    }
}

export default UserController
