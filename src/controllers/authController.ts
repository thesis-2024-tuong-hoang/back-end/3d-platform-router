import 'dotenv/config'
import * as express from 'express'
import { IUserRegister, IUserLogin, ILoginResponse } from '~/type'
import { AUTHENTICATION_URL } from '~/constants/constants'
import axios from 'axios'

class AuthController {
    static async register(req: express.Request, res: express.Response, next: express.NextFunction) {
        const registerInput = req.body as IUserRegister

        try {
            const response = await axios({
                method: 'post',
                url: AUTHENTICATION_URL + '/register',
                data: registerInput
            })

            if (response.status === 201) {
                res.status(response.status).json({
                    message: 'Creating a new user successfully'
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async login(req: express.Request, res: express.Response, next: express.NextFunction) {
        const loginInput = req.body as IUserLogin
        try {
            const response: ILoginResponse = await axios({
                method: 'post',
                url: AUTHENTICATION_URL + '/login',
                data: loginInput
            })
            // console.log(response)
            if (response.status === 200) {
                res.status(response.status).json({
                    accessToken: response.data.accessToken,
                    refreshToken: response.data.refreshToken,
                    userId: response.data.userId,
                    message: 'Login successfully'
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async loginWithGoogle(req: express.Request, res: express.Response, next: express.NextFunction) {
        const data = req.body as any
        // console.log(data)
        try {
            const response: any = await axios({
                method: 'post',
                url: AUTHENTICATION_URL + '/login-with-google',
                data: data
            })
            console.log(response)
            if (response.status === 200) {
                res.status(response.status).json({
                    accessToken: response.data.accessToken,
                    refreshToken: response.data.refreshToken,
                    userId: response.data.userId,
                    message: 'Login with Google successfully'
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async logout(req: express.Request, res: express.Response, next: express.NextFunction) {
        const userId = req.body.id 

        try {
            const response: ILoginResponse = await axios({
                method: 'post',
                url: AUTHENTICATION_URL + '/logout',
                data: {
                    id: userId
                }
            })

            if (response.status === 200) {
                res.clearCookie('accessToken').clearCookie('refreshToken')
                res.status(response.status).json({ message: response.data.message })
            }
        } catch (error: any) {
            if (!error.status) {
                error.status = 500
            }
            next(error)
        }
    }

    static async isAuthorized(req: express.Request, res: express.Response, next: express.NextFunction) {
        const fullAccessToken = req.get('Authorization-1')?.split(' ')[1]
        const fullRefreshToken = req.get('Authorization-2')?.split(' ')[1]
     

        try {
            const response = await axios({
                method: 'post',
                url: AUTHENTICATION_URL + '/authorize',
                data: {
                    accessToken: fullAccessToken,
                    refreshToken: fullRefreshToken
                }
            })
            if (response.status === 201) {
                res.status(201).json({
                    accessToken: response.data.newAccessToken,
                    refreshToken: response.data.newRefreshToken,
                    isAuthorized: response.data.isAuthorized,
                    message: response.data.message
                })
            }
        } catch (error: any) {
            if (error) {
                console.log('Token expired')
            }
            if (!error.status) {
                error.status = 500
            }
            next(error)
        }
    }
}

export default AuthController
