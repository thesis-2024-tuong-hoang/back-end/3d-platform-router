import 'dotenv/config'
import * as express from 'express'
import { IUserRegister, IUserLogin, ILoginResponse } from '~/type'
import { DATA_PROCESSOR_URL } from '~/constants/constants'
import axios from 'axios'

interface IModelBody {
    description: string
    name: string
    userId: string
}

class ModelController {
    static async uploadNewModalInfo(req: express.Request, res: express.Response, next: express.NextFunction) {
        const modelData = req.body as IModelBody

        try {
            const response = await axios({
                method: 'post',
                url: DATA_PROCESSOR_URL + '/upload-model',
                data: modelData
            })

            if (response.status === 201) {
                res.status(response.status).json({
                    message: 'Upload a new model successfully'
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async getAllModals(req: express.Request, res: express.Response, next: express.NextFunction) {
        try {
            const response = await axios({
                method: 'GET',
                url: DATA_PROCESSOR_URL + `/3d-models/all`
            })

            console.log(response.data)

            if (response.status === 200) {
                res.status(response.status).json({
                    models: response.data.models
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async getAllModalsByUserId(req: express.Request, res: express.Response, next: express.NextFunction) {
        const userId = req.params.userId

        try {
            const response = await axios({
                method: 'GET',
                url: DATA_PROCESSOR_URL + `/3d-models/all/${userId}`
            })

            console.log(response.data)

            if (response.data.status === 200) {
                res.status(response.status).json({
                    models: response.data.models
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async getModalByName(req: express.Request, res: express.Response, next: express.NextFunction) {
        const modelName = req.params.modelName as any

        const modelNamePath = modelName.split(' ').join('-')

        try {
            const response = await axios({
                method: 'GET',
                url: DATA_PROCESSOR_URL + `/3d-models/${modelNamePath}`
            })

            if (response.status === 200) {
                res.status(response.status).json({
                    model: response.data.model,
                    user: response.data.user
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async getModalsByUserId(req: express.Request, res: express.Response, next: express.NextFunction) {
        const userId = req.body.currentUser

        try {
            const response = await axios({
                method: 'POST',
                url: DATA_PROCESSOR_URL + `/3d-models`,
                data: { userId: userId }
            })

            if (response.status === 200) {
                res.status(response.status).json({
                    modelList: response.data.modelList,
                    user: response.data.user
                })
            }
        } catch (error) {
            next(error)
        }
    }
}

export default ModelController
