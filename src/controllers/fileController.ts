import * as express from 'express'
import { ErrorResponse } from '~/utils/errors'
import { FILE_PROCESSOR_URL } from '~/constants/constants'
import { validationResult } from 'express-validator'
import axios from 'axios'

class FileController {
    static async upload(req: express.Request, res: express.Response, next: express.NextFunction) {
        const gltfFiles = req.files as Express.Multer.File[]
        const modelName = req.params.model

        try {
            const response = await axios({
                method: 'post',
                url: FILE_PROCESSOR_URL + '/upload/' + modelName,
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                data: gltfFiles
            })
            // console.log(response)
            // gltfFiles.forEach(async (file) => {
            //     const newBlob = BufferToBlob(file.buffer, file.mimetype)
            //     const path = generatePathForUpload(req.params.model, file)
            //     const response = await uploadFile('3D-models', path, newBlob, file.mimetype)
            //     if (!response) {
            //         const newError = ErrorResponse(
            //             `Cannot save file ${file.originalname} on database`,
            //             500,
            //             undefined,
            //             undefined
            //         )
            //         throw newError
            //     }
            // })
            // res.status(200).json({ message: 'Save new model successfully !' })
        } catch (error) {
            next(error)
        }
    }
    static async download() {}
}

export default FileController
