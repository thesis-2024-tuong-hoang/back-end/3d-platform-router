export interface ILoginResponse {
    status: number
    data: {
        accessToken: string
        refreshToken: string
        userId: string
        message: string
    }
}

export interface ITokenDecodedPayload {
    userId: string
    name: string
    iat: number
    exp: number
}

export interface ITokenRecord {
    token: string
    user_id: string
}

export interface IPayloadToken {
    userId: string
    name: string
}

export interface IUserRegister {
    email: string
    password: string
    firstname: string
    lastname: string
}

export interface IUserLogin {
    email: string
    password: string
}

export interface IUserInfo {
    email: string
    firstname: string
    lastname: string
}

export interface IUserCredential {
    email: string
    password: string
    user_id: string
}

export interface IResponseError extends Error {
    status?: number
    messages?: { [errorName: string]: string }
}
