import { Router } from 'express'
import fileController from '~/controllers/fileController'

const router = Router()

router.post('/upload/:model', fileController.upload)

router.post('/download', fileController.download)

export default router
