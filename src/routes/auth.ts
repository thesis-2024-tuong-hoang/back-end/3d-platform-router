import { Router } from 'express'
import AuthController from '~/controllers/authController'

const router = Router()

router.post('/register', AuthController.register)

router.post('/login', AuthController.login)

router.post('/login-with-google', AuthController.loginWithGoogle)

router.post('/logout', AuthController.logout)

router.post('/authorize', AuthController.isAuthorized)

export default router
