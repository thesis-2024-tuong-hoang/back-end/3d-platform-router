import { Router } from 'express'
import ModelController from '~/controllers/modelController'
import UserController from '~/controllers/userController'

const router = Router()

router.post('/upload-model', ModelController.uploadNewModalInfo)

router.get('/3d-models/all', ModelController.getAllModals)

router.get('/3d-models/all/:userId', ModelController.getAllModalsByUserId)

router.post('/3d-models', ModelController.getModalsByUserId)

router.get('/3d-models/:modelName', ModelController.getModalByName)

router.get('/user/:userSlug', UserController.getUserBySlug)

router.post('/user', UserController.getUserById)

router.put('/user', UserController.updateUserById)

export default router
