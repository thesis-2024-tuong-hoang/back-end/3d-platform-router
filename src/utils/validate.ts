import { Schema } from 'express-validator'

export default class ValidateSchemas {
    static register: Schema = {
        email: {
            isEmail: true,
            notEmpty: true,
            errorMessage: 'Email is not valid.'
        },
        password: {
            isAlphanumeric: true,
            isLength: {
                options: {
                    min: 6
                }
            },
            notEmpty: true,
            errorMessage: 'Password must be greater then 6 characters.'
        },
        firstname: {
            isString: true,
            notEmpty: true
        },
        lastname: {
            isString: true,
            notEmpty: true
        }
    }
    static login: Schema = {
        email: {
            isEmail: true,
            notEmpty: true,
            errorMessage: 'Email is not valid.'
        },
        password: {
            isAlphanumeric: true,
            isLength: {
                options: {
                    min: 6
                }
            },
            notEmpty: true,
            errorMessage: 'Password must be greater then 6 characters.'
        }
    }
}
