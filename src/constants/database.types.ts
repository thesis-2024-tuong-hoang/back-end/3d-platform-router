export type Json = string | number | boolean | null | { [key: string]: Json | undefined } | Json[]

export interface Database {
    public: {
        Tables: {
            model: {
                Row: {
                    created_at: string | null
                    id: number
                    name: string | null
                }
                Insert: {
                    created_at?: string | null
                    id?: number
                    name?: string | null
                }
                Update: {
                    created_at?: string | null
                    id?: number
                    name?: string | null
                }
                Relationships: []
            }
            user: {
                Row: {
                    created_at: string
                    email: string | null
                    firstname: string | null
                    id: string
                    lastname: string | null
                }
                Insert: {
                    created_at?: string
                    email?: string | null
                    firstname?: string | null
                    id?: string
                    lastname?: string | null
                }
                Update: {
                    created_at?: string
                    email?: string | null
                    firstname?: string | null
                    id?: string
                    lastname?: string | null
                }
                Relationships: []
            }
        }
        Views: {
            [_ in never]: never
        }
        Functions: {
            [_ in never]: never
        }
        Enums: {
            [_ in never]: never
        }
        CompositeTypes: {
            [_ in never]: never
        }
    }
}
