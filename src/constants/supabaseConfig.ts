import 'dotenv/config'
import { createClient } from '@supabase/supabase-js'
import { Database } from './database.types'
import { SUPABASE_URL, SUPABASE_ANON_KEY } from './constants'

const supabase = createClient<Database>(SUPABASE_URL, SUPABASE_ANON_KEY!)

export default supabase
